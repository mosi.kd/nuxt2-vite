import { DefinePlugin, ProvidePlugin } from 'webpack';
import UnhandledRejection from '@bugsnag/plugin-window-unhandled-rejection';
import { BugsnagSourceMapUploaderPlugin } from 'webpack-bugsnag-plugins';
import { version } from './package.json';

const pkg = {
  title: 'Jack Westin the MCAT CARS Tutor',
  description: process.env.npm_package_description,
  author: 'Jack Westin',
};

const isDev = process.env.NODE_ENV === 'development';
const isProduction = process.env.NODE_ENV === 'production';

const config = {
  ssr: false,

  components: [
    {
      path: '~/components',
      pathPrefix: false,
    },
  ],

  head: {
    title: process.env.npm_package_title || '',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        hid: 'og:image:height',
        property: 'og:image:height',
        content: '588',
      },
      {
        hid: 'og:image:width',
        property: 'og:image:width',
        content: '1123',
      },
      {
        hid: 'description',
        name: 'description',
        content:
          'Take an online course with a top MCAT CARS tutor to improve your verbal score. Jack helps students from Toronto to Los Angeles and around the world. Sign up here!',
      },
      { hid: 'author', name: 'author', content: pkg.author },
      { hid: 'theme-color', name: 'theme-color', content: '#4C51BF' },
      {
        hid: 'apple-mobile-web-app-title',
        name: 'apple-mobile-web-app-title',
        content: pkg.title,
      },
      ...[
        // pwa meta for apple devices
        { name: 'apple-mobile-web-app-title', content: 'Jack Westin' },
        { name: 'apple-mobile-web-app-capable', content: 'yes' },
        { name: 'theme-color', content: '#4f5dbc' },
        { name: 'apple-mobile-web-app-status-bar-style', content: '#4f5dbc' },
      ],
      { name: 'google-site-verification', content: 'uc1ZMq3nAznKkDgsdRrJrA7mDGKjli1PRijfqjCSSdU' },
      { name: 'facebook-domain-verification', content: '6axwlmid0xixsj63youje325g5d3ps' },
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Nunito:300,400,700&display=swap' },
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css2?family=Inter:wght@700&display=swap' },
    ],
  },

  css: [
    '~/assets/css/styles.css',
    '@fortawesome/fontawesome-svg-core/styles.css',
    './node_modules/quill/dist/quill.snow.css',
  ],

  ignore: [
    '**/*.test.*',
    '**/*.spec.*',
    'pages/mcat-cyber-month/*',
    'components/landings/mcat-cyber-month/*',
    'components/ticket/*',
  ],

  plugins: [
    '@/plugins/api/axios.js',
    '@/plugins/api/apiFactory.js',
    '@/plugins/api/apiUtil.js',
    '@/plugins/datetime/index.client.js',
    '@/plugins/directives/index.client.js',
    '@/plugins/jw-toast/index.js',
    '@/plugins/ui/index.client.js',
    '@/plugins/vendors/index.client.js',
    '@/plugins/config.js',
    '@/plugins/filters.client.js',
    { src: '@/plugins/hubspot', mode: 'client' },
    '@/plugins/routerUtil.js',
    '@/plugins/services.js',
    '@/plugins/stripe-plugin.client.js',
    '@/plugins/vcalendar.client.js',
    '@/plugins/log.js',
    '@/plugins/third-party-client-scripts.js',
    // '@/plugins/sw',
  ],

  buildModules: [
    'nuxt-vite',
    '@nuxtjs/recaptcha',
    '@nuxtjs/tailwindcss',
    '@nuxtjs/moment',
    [
      '@nuxtjs/laravel-echo',
      {
        broadcaster: 'socket.io',
        authModule: true,
        connectOnLogin: true,
        disconnectOnLogout: true,
      },
    ],
  ],
  vite: {
    // Vite-specific configurations here
    ssr: false,
    server: {
      hmr: {
        overlay: false,
      },
    },
  },

  modules: [
    '@nuxtjs/axios',
    '@nuxtjs/auth-next',
    'nuxt-route-meta',
    '@blokwise/dynamic',
    '@nuxtjs/google-gtag',
    '@nuxtjs/gtm',
    '@nuxtjs/toast',
    '@nuxtjs/device',
    'vue-social-sharing/nuxt',
    '@nuxtjs/robots',
    // ['cookie-universal-nuxt', { alias: 'cookiz' }],
    [
      'nuxt-bugsnag',
      {
        config: {
          version,
          appVersion: version,
          releaseStage: process.env.NODE_ENV,
          plugins: [UnhandledRejection],
        },
        apiKey: process.env.BUGSNAG_KEY,
        appType: 'client',
        appVersion: version,
        enabledReleaseStages: ['staging', 'production'],
        releaseStage: process.env.NODE_ENV,
        publishRelease: !isDev,
        reporterOptions: {
          appVersion: version,
          releaseStage: process.env.NODE_ENV,
          autoAssignRelease: !isDev,
        },
      },
    ],
    'nuxt-socket-io',
    [
      'nuxt-lazy-load',
      {
        directiveOnly: true,
        loadingClass: 'lazy-loading',
      },
    ],
    'nuxt-facebook-pixel-module',
  ],

  io: {
    // Required
    sockets: [
      {
        // At least one entry is required
        name: 'socket.io',
      },
    ],
  },

  robots: [
    {
      UserAgent: '*',
      Disallow: ['/programs', '/spark', '/dashboard'],
    },
    {
      UserAgent: [
        'ahrefsbot',
        'RogerBot',
        'MJ12bot',
        'Screaming Frog SEO Spider',
        'DotBot',
        'SemrushBot-SA',
        'Yandex',
        'Mail.Ru',
        'Baidu',
        'Sogou',
        'Youdao',
        'ChinasoSpider',
        'Sosospider',
        'yisouspider',
        'Haosou',
        'moget',
        'ichiro',
        'NaverBot',
        'Daumoa',
        'Yeti',
      ],
      Disallow: '/',
    },
  ],

  'google-gtag': {
    id: process.env.GA_TRACKING_CODE,
    debug: isDev,
  },

  gtm: {
    enabled: true,
    debug: isDev,
    autoInit: false,
    id: process.env.GA_GTM_KEY,
    scriptURL: 'https://www.googletagmanager.com/gtag/js',
  },

  publicRuntimeConfig: {
    APP_VERSION: version,
    API_URL: process.env.API_URL,
    MOCK_API_PORT: process.env.MOCK_API_PORT,
    gtm: {
      id: process.env.GA_GTM_KEY,
    },
    GA_TRACKING_CODE: process.env.GA_TRACKING_CODE,
    STRIPE_KEY: process.env.STRIPE_KEY,
    NODE_ENV: process.env.NODE_ENV,
    SHARE_A_SALE_MERCHANT_ID: process.env.SHARE_A_SALE_MERCHANT_ID,
    SITE_URL: process.env.SITE_URL,
    FLASHCARD_IFRAME_URL: process.env.FLASHCARD_IFRAME_URL,
    recaptcha: {
      version: 3,
      hideBadge: true,
      siteKey: process.env.RECAPTCHA_SITE_KEY,
    },
    firebase: {
      apiKey: process.env.FIREBASE_API_KEY,
      authDomain: process.env.FIREBASE_AUTH_DOMAIN,
      projectId: process.env.FIREBASE_PROJECT_ID,
      storageBucket: process.env.FIREBASE_STORAGE_BUCKET,
      messagingSenderId: process.env.FIREBASE_MESSAGING_SENDER_ID,
      appId: process.env.FIREBASE_APP_ID,
      measurementId: process.env.FIREBASE_MEASUREMENT_ID,
    },
    HUBSPOT_CHAT_BOX: process.env.HUBSPOT_CHAT_BOX,
    PDF_USER_PASSWORD: process.env.PDF_USER_PASSWORD,
    DAT_MENU_PAGE_URL: process.env.DAT_MENU_PAGE_URL,
  },

  moment: {
    timezone: true,
  },

  toast: {
    position: 'top-center',
    duration: 3500,
    keepOnHover: true,
  },

  axios: {
    proxy: false,
    credentials: true,
  },

  proxy: {
    '/api': {
      target: process.env.API_URL,
    },
  },

  auth: {
    redirect: false,
    strategies: {
      sanctum: {
        provider: 'laravel/sanctum',
        url: process.env.API_URL,
        user: {
          autoFetch: false,
        },
        endpoints: {
          login: {
            url: '/api/caap/login',
            method: 'post',
          },
          logout: {
            url: '/api/caap/logout',
            method: 'post',
          },
          user: {
            url: '/api/caap/me',
            method: 'get',
            propertyName: false,
          },
        },
      },
    },
    plugins: ['~/plugins/gtm', '~/plugins/jackwestin-uikit.js'],
  },

  storybook: {
    stories: ['~/stories/**/*.stories.js', '~/components/**/*.stories.js'],
  },

  facebook: {
    track: 'PageView',
    pixelId: '738521281081385',
    autoPageView: true,
    disabled: false,
    debug: process.env.NODE_ENV !== 'production',
  },

  loadingIndicator: '~/loading.html',

  build: {
    cache: true,
    parallel: true,
    hardSource: true,
    plugins: [
      new DefinePlugin({
        PRODUCTION: JSON.stringify(process.env.NODE_ENV === 'production'),
        'process.VERSION': version,
        'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV),
      }),
      new ProvidePlugin({ THREE: 'three' }),
    ],
  },

  router: {
    prefetchLinks: true,
    base: '/',
    middleware: ['ga', 'me', 'url'],
  },
};

if (isProduction) {
  config.ignore.push('pages/demo/*');
}
if (!isDev) {
  config.build.plugins.push(
    // Upload source map to bugsnag
    new BugsnagSourceMapUploaderPlugin({
      apiKey: process.env.BUGSNAG_KEY,
      appVersion: version,
      overwrite: true,
    }),
  );
}

export default config;
