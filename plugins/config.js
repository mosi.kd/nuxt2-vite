import { configs } from '~/core/configs/configs';

export default (_, inject) => {
  inject('config', Object.freeze(configs));
};
