export default ({ $auth, app }, inject) => {
  const gaConfig = {
    items: {
      navbar: {
        logo: {
          eventCategory: 'navbar',
          eventAction: 'goto',
          eventLabel: 'logo',
        },
        home: {
          eventCategory: 'navbar',
          eventAction: 'goto',
          eventLabel: 'home',
        },
        courses: {
          eventCategory: 'navbar',
          eventAction: 'goto',
          eventLabel: 'courses',
        },
        sessions: {
          eventCategory: 'navbar',
          eventAction: 'goto',
          eventLabel: 'sessions',
        },
        coaching: {
          eventCategory: 'navbar',
          eventAction: 'goto',
          eventLabel: 'coaching',
        },
        admissions: {
          eventCategory: 'navbar',
          eventAction: 'goto',
          eventLabel: 'admissions',
        },
        tutoring: {
          eventCategory: 'navbar',
          eventAction: 'goto',
          eventLabel: 'tutoring',
        },
      },
      marketing: {
        promotional_video: {
          eventCategory: 'marketing',
          eventAction: 'play',
          eventLabel: 'promotional-video',
        },
        home_banner: {
          eventCategory: 'banner',
          eventAction: 'goto',
          eventLabel: 'marketing',
          eventValue: 4,
        },
        upgrade_course: {
          eventCategory: 'upgrade-option',
          eventAction: 'goto',
          eventLabel: 'marketing',
          eventValue: 1,
        },
        fts_enrollment: {
          eventCategory: 'Lead Generation',
          eventAction: 'Form Submissions',
          eventLabel: 'FTS Form',
          eventValue: 1,
        },
      },
      sidebar: {
        home: {
          eventCategory: 'sidebar',
          eventAction: 'goto',
          eventLabel: 'home',
        },
        courses: {
          eventCategory: 'sidebar',
          eventAction: 'goto',
          eventLabel: 'courses',
        },
        sessions: {
          eventCategory: 'sidebar',
          eventAction: 'goto',
          eventLabel: 'sessions',
        },
        coaching: {
          eventCategory: 'sidebar',
          eventAction: 'goto',
          eventLabel: 'coaching',
        },
        admissions: {
          eventCategory: 'sidebar',
          eventAction: 'goto',
          eventLabel: 'admissions',
        },
        tutoring: {
          eventCategory: 'sidebar',
          eventAction: 'goto',
          eventLabel: 'tutoring',
        },
        analytics: {
          eventCategory: 'sidebar',
          eventAction: 'goto',
          eventLabel: 'analytics',
        },
        mcat: {
          eventCategory: 'sidebar',
          eventAction: 'goto',
          eventLabel: 'MCAT Practice Passages',
        },
        cars: {
          eventCategory: 'sidebar',
          eventAction: 'goto',
          eventLabel: 'CARS Practice Exams',
        },
        khan: {
          eventCategory: 'sidebar',
          eventAction: 'goto',
          eventLabel: 'Khan Academy',
        },
        outline: {
          eventCategory: 'sidebar',
          eventAction: 'goto',
          eventLabel: 'AAMC Outline',
        },
        retake: {
          eventCategory: 'sidebar',
          eventAction: 'goto',
          eventLabel: 'Retake Calculator',
        },
        chrome: {
          eventCategory: 'sidebar',
          eventAction: 'goto',
          eventLabel: 'AAMC Chrome Extension',
        },
        reviews: {
          eventCategory: 'sidebar',
          eventAction: 'goto',
          eventLabel: 'Reviews',
        },
        flashcards: {
          eventCategory: 'sidebar',
          eventAction: 'goto',
          eventLabel: 'Flashcards',
        },
        about: {
          eventCategory: 'sidebar',
          eventAction: 'goto',
          eventLabel: 'About Jack Westin',
        },
        faq: {
          eventCategory: 'sidebar',
          eventAction: 'goto',
          eventLabel: 'Faq',
        },
        blog: {
          eventCategory: 'sidebar',
          eventAction: 'goto',
          eventLabel: 'Blog',
        },
        contact: {
          eventCategory: 'sidebar',
          eventAction: 'goto',
          eventLabel: 'Contact',
        },
        support: {
          eventCategory: 'sidebar',
          eventAction: 'goto',
          eventLabel: 'Support',
        },
        privacy: {
          eventCategory: 'sidebar',
          eventAction: 'goto',
          eventLabel: 'Privacy Policy - Terms',
        },
        seemore: {
          eventCategory: 'sidebar',
          eventAction: 'Toggle',
          eventLabel: 'See more',
        },
        seeless: {
          eventCategory: 'sidebar',
          eventAction: 'Toggle',
          eventLabel: 'See less',
        },
      },
      user: {
        eventCategory: 'init',
        eventAction: 'set',
        eventLabel: 'Used id',
      },
      share: {
        facebook: {
          eventCategory: 'marketing',
          eventAction: 'share',
          eventLabel: 'facebook',
        },
        twitter: {
          eventCategory: 'marketing',
          eventAction: 'share',
          eventLabel: 'twitter',
        },
        email: {
          eventCategory: 'marketing',
          eventAction: 'share',
          eventLabel: 'email',
        },
        link: {
          eventCategory: 'marketing',
          eventAction: 'share',
          eventLabel: 'link',
        },
      },
    },
  };

  /**
    This is called in templates
   **/
  const gaPush = (category, key, props) => {
    const { eventCategory, eventAction, eventLabel, eventValue } = gaConfig.items[category][key];
    const actionName = props?.eventAction ?? eventAction;

    const payload = {
      event_category: props?.eventCategory ?? eventCategory,
      event_label: props?.eventLabel ?? eventLabel,
      value: props?.eventValue || eventValue || '',
    };

    app.$gtag('event', actionName, payload);
  };

  if ($auth.user?.id) {
    setTimeout(() => {
      app.$gtag('config', {
        user_id: String($auth.user.id),
      });
    }, 200);
  }

  // Make `gaConfig` and `gaPush` globally referenced
  inject('gaConfig', gaConfig);
  inject('gaPush', gaPush);
};
