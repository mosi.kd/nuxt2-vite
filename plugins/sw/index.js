/* eslint-disable no-console */
export default function () {
  if ('serviceWorker' in navigator) {
    navigator.serviceWorker
      .register('/service-worker.js')
      .then(() => {
        console.log('Service worker registration succeeded');
      })
      .catch((error) => {
        console.error('Service worker registration failed:', error);
      });
  } else {
    console.error('Service workers are not supported.');
  }
}
