import Vue from 'vue';
import { StripePlugin } from '@vue-stripe/vue-stripe';

export default ({ $config }, inject) => {
  const use = () => {
    const options = {
      pk: $config.STRIPE_KEY,
    };
    Vue.use(StripePlugin, options);
  };

  const stripeProvidingPromise = () => {
    return new Promise((resolve) => {
      if (window.Stripe) {
        return resolve();
      } else {
        const interval = setInterval(() => {
          if (window.Stripe) {
            clearInterval(interval);
            return resolve();
          }
        }, 50);
      }
    });
  };

  const makeSureStripeInjected = () => {
    return new Promise((resolve) => {
      if (window.$nuxt.$stripe) {
        return resolve();
      } else {
        const interval = setInterval(() => {
          if (window.$nuxt.$stripe) {
            clearInterval(interval);
            return resolve();
          }
        }, 50);
      }
    });
  };

  stripeProvidingPromise().then(() => use());
  inject('makeSureStripeInjected', makeSureStripeInjected);
};
