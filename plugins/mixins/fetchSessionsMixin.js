export const fetchSessionsMixin = {
  methods: {
    async onFetchSessions(sortBy) {
      const query = { ...this.$store.state.session.params };
      query.order = sortBy;
      query.course = this.course.id;
      query.type = null;
      await this.$store.dispatch('session/fetchSessions', query);
    },
  },
};
