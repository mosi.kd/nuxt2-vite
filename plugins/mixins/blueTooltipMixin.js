export const blueTooltipMixin = {
  head() {
    return {
      bodyAttrs: {
        class: 'tooltip-blue-wrapper',
      },
    };
  },
};
