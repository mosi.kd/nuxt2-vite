import sumBy from 'lodash/sumBy';
import { mapGetters } from 'vuex';

import { courseMixin } from '~/plugins/mixins/courseMixin';

export const lecturesMixin = {
  mixins: [courseMixin],
  data() {
    return {
      lecturesCount: [],
    };
  },
  computed: {
    ...mapGetters('course', ['course']),
    lectures() {
      return sumBy(this.lecturesCount, 'lecture');
    },
    homeworks() {
      return sumBy(this.lecturesCount, 'homework');
    },
    workshops() {
      return sumBy(this.lecturesCount, 'workshop');
    },
    sections() {
      return this.course?.sections?.length ?? 0;
    },
  },
  watch: {
    'course.sections': {
      immediate: true,
      deep: true,
      handler(value) {
        if (value !== undefined) {
          this.lecturesCount = this.getCourseLectureCounts(JSON.parse(JSON.stringify(value)));
        }
      },
    },
  },
};
