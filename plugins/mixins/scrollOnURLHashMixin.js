export const scrollOnURLHashMixin = {
  methods: {
    scrollOnURLHash() {
      if (window.location.hash === this.$route.hash) {
        const el = document.getElementById(this.$route.hash.slice(1));
        if (el) {
          window.scrollTo(0, el.offsetTop);
        }
      }
    },
  },
};
