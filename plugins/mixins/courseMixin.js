import * as courseManagement from '~/core/management/courseManagement';
export const courseMixin = {
  methods: {
    getCourseTotalTime: (sections) => {
      return courseManagement.getCourseTotalTime(sections);
    },
    getCourseOnDemandVideo: (sections) => {
      return courseManagement.getCourseOnDemandVideo(sections);
    },
    getSectionTotalTime: (section) => {
      return courseManagement.getSectionTotalTime(section);
    },
    getCourseLessonsCount: (sections) => {
      return courseManagement.getCourseLessonsCount(sections);
    },
    getCompletedLessons: (sections) => {
      return courseManagement.getCompletedLessons(sections);
    },
    getSectionCompletedLessons: (section) => {
      return courseManagement.getSectionCompletedLessons(section).length;
    },
    getSectionLectureCounts: (section) => {
      return courseManagement.getSectionLectureCounts(section);
    },
    getCourseLectureCounts: (sections) => {
      return courseManagement.getCourseLectureCounts(sections);
    },
    getLectureCountsString: (count) => {
      return courseManagement.getLectureCountsString(count);
    },
  },
};
