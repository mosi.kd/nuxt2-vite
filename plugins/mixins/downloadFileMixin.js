export const downloadFileMixin = {
  methods: {
    downloadFile(id, url) {
      return new Promise((resolve, reject) => {
        this.$axios({
          method: 'get',
          url: `/api/caap/resources/${id}/download`,
          responseType: 'arraybuffer',
        })
          .then((res) => {
            const filename = url.split('/').pop();
            const objectUrl = window.URL.createObjectURL(new Blob([res]));

            const link = document.createElement('a');
            link.href = objectUrl;
            link.setAttribute('download', filename);
            document.body.appendChild(link);
            link.click();
            resolve(true);
          })
          .catch((e) => {
            reject(e);
          });
      });
    },
  },
};
