export default {
  props: {
    value: {
      type: [String, Boolean, Number, Object, null],
      default: null,
    },
  },
  data() {
    return {
      errors: {},
    };
  },
  methods: {
    handleErrors(err) {
      this.errors = this.$getErrors(err) || this.errors;
    },
  },
};
