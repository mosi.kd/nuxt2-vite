import { getErrors } from '~/core/utils/apiUtil';

export default (_, inject) => {
  inject('getErrors', getErrors);
};
