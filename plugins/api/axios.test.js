import { postApiError } from './axios';

const app = {
  $auth: {
    user: {
      id: 1,
      email: 'behzad.am@gmail.com',
      name: 'Behzad',
    },
  },
};

const $bugsnag = {
  notify: jest.fn(),
};

const error = {
  response: {
    data: { message: 'Server is not available' },
  },
};

const $config = { NODE_ENV: 'production' };

describe('Bugsnag Axios', () => {
  test('should post error message', () => {
    postApiError(app, $bugsnag, error, 500, $config);
    expect($bugsnag.notify).toHaveBeenCalledTimes(1);
  });
});
