import Vue from 'vue';
import { BREAK_POINTS } from '~/core/utils/constants';

const imageBlacklist = ['btn', 'jw-expanded-successful'];

let observer = null;

const loadBackground = () => {
  if (!document.getElementById('jw-expandable-background')) {
    const backgroundDiv = document.createElement('div');
    backgroundDiv.id = 'jw-expandable-background';
    backgroundDiv.className =
      'fixed flex justify-center items-center top-0 left-0 w-screen h-screen z-[999] bg-black/50';
    backgroundDiv.style.cssText = 'visibility: hidden; opacity: 0; transition: all ease-in-out 0.4s;';
    backgroundDiv.addEventListener('click', function (event) {
      if (event.target.id === 'jw-expandable-background') {
        this.style.visibility = 'hidden';
        this.style.opacity = '0';
      }
    });

    const imageDiv = document.createElement('div');
    imageDiv.id = 'jw-expandable-wrapper';
    imageDiv.className = 'relative max-h-screen overflow-y-auto pt-11 px-4 pb-4 bg-[#006DAA]';
    backgroundDiv.appendChild(imageDiv);
    document.body.appendChild(backgroundDiv);
  }
};

const isMobileView = () => window.innerWidth <= BREAK_POINTS.MD;

const zoomImg = function () {
  const closeElement = document.createElement('div');
  closeElement.id = 'close-element';
  closeElement.style.cssText = 'line-height: 13px; cursor: pointer; text-align: center;';
  closeElement.className = 'w-4 h-4 border-2 absolute top-3 right-3 text-white';
  closeElement.innerHTML = 'X';

  // Create evil image clone
  const clone = this.cloneNode();

  // Put evil clone into lightbox
  let lb = document.getElementById('jw-expandable-wrapper');
  lb.innerHTML = '';
  lb.appendChild(clone);
  lb.appendChild(closeElement);

  clone.style.width = isMobileView() ? `${window.innerWidth - 40}px` : `${(window.innerWidth * 80) / 100}px`;
  clone.style.maxHeight = `${window.innerHeight - (window.innerHeight * 20) / 100}px`;
  clone.style.maxWidth = isMobileView() ? '100%' : `${window.innerWidth - 90}px`;
  clone.classList.add('jw-expanded-successful');
  clone.style.cursor = 'initial';

  // Show lightbox
  lb = document.getElementById('jw-expandable-background');
  lb.style.visibility = 'visible';
  lb.style.opacity = '1';
};

const makeImageExpandable = (img) => {
  if (imageBlacklist.every((className) => !img.classList.contains(className))) {
    img.style.cursor = 'zoom-in';
    img.addEventListener('load', () => {
      img.addEventListener('click', zoomImg);
    });
  }
};

const observesOnChildNodes = (el) => {
  observer = new MutationObserver(function (mutations) {
    mutations.forEach(function (mutation) {
      for (let i = 0; i < mutation.addedNodes.length; i++) {
        mutation.target.querySelectorAll('img').forEach((img) => {
          makeImageExpandable(img);
        });
      }
    });
  });
  observer.observe(el, { childList: true, subtree: true });
};

// sample:
// v-enlarge-images"
Vue.directive('enlarge-images', {
  inserted(el) {
    loadBackground();
    observesOnChildNodes(el);
    el.querySelectorAll('img').forEach((img) => {
      makeImageExpandable(img);
    });
  },
  unbind() {
    observer?.disconnect();
    observer = null;
  },
});
