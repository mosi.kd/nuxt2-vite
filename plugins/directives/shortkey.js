import Vue from 'vue';

function getKeyCode(expression) {
  if (!expression) {
    return expression;
  }
  if (expression.length > 1) {
    switch (expression) {
      case 'up':
        return 38;
      case 'down':
        return 40;
      case 'space':
        return 32;
    }
  }

  return expression?.toUpperCase()?.charCodeAt(0);
}

let event;
Vue.directive('shortkey', {
  inserted(el, binding, vnode) {
    event = function (e) {
      if (el.disabled) {
        return;
      }

      if (!!binding.modifiers.alt === !e.altKey) {
        return;
      }
      if (!!binding.modifiers.ctrl === !e.ctrlKey) {
        return;
      }

      let keyCode;
      if (typeof binding.value === 'string') {
        keyCode = getKeyCode(binding.value);
      } else if (Array.isArray(binding.value)) {
        keyCode = getKeyCode(binding.value[0]);
      }

      if (e.keyCode !== keyCode) {
        return;
      }
      vnode.data.on.shortkey(el);
    };

    document.addEventListener('keydown', event);
  },
  unbind() {
    if (event) {
      document.removeEventListener('keydown', event);
    }
  },
});
