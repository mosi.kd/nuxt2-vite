import { faFacebookF } from '@fortawesome/free-brands-svg-icons/faFacebookF';
import { faGoogle } from '@fortawesome/free-brands-svg-icons/faGoogle';
import { faFacebookSquare } from '@fortawesome/free-brands-svg-icons/faFacebookSquare';
import { faGooglePlusSquare } from '@fortawesome/free-brands-svg-icons/faGooglePlusSquare';
import { faTwitter } from '@fortawesome/free-brands-svg-icons/faTwitter';

const brandIcons = [faFacebookF, faGoogle, faFacebookSquare, faGooglePlusSquare, faTwitter];

export default brandIcons;
