import { faCalendar } from '@fortawesome/free-regular-svg-icons/faCalendar';
import { faCalendarAlt } from '@fortawesome/free-regular-svg-icons/faCalendarAlt';
import { faClock } from '@fortawesome/free-regular-svg-icons/faClock';
import { faPlayCircle } from '@fortawesome/free-regular-svg-icons/faPlayCircle';

const regularIcons = [faCalendar, faClock, faCalendarAlt, faPlayCircle];

export default regularIcons;
