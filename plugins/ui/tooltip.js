import Vue from 'vue';
import Tooltip from 'v-tooltip';
import './tooltip.css';

Vue.use(Tooltip, {
  defaultTrigger: 'hover click',
});
