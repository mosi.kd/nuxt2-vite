import Vue from 'vue';
import { Fragment } from 'vue-fragment';

require('./scrollActive');
require('./tooltip');
Vue.component('Fragment', Fragment);
