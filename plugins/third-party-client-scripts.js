export default ({ app, route, $log }) => {
  const scripts = {
    stripe: {
      src: 'https://js.stripe.com/v3',
      async: 'async',
      defer: 'defer',
      id: 'stripe-js',
      immediateInjectOn: [
        'tier-id-checkout',
        'tier-id-checkout-callback',
        'class-id-checkout',
        'class-id-checkout-callback',
        'student-index',
        'class-id-checkout-thank-you',
        'tier-id-checkout-thank-you',
        'student-general',
        'student-index-tire-id',
        'student-index-tire-id-payments',
        'student-index-course-id-payments',
        'courses-slug',
        'programs-slug',
      ],
    },
    dwin1: {
      src: 'https://www.dwin1.com/19038.js',
      async: 'async',
      defer: 'defer',
      id: 'dwin1-js',
      immediateInjectOn: '*',
    },
  };

  function onDocumentIsReady(fn) {
    // see if DOM is already available
    if (document.readyState === 'complete' || document.readyState === 'interactive') {
      // call on next available tick
      setTimeout(fn, 1);
    } else {
      document.addEventListener('DOMContentLoaded', fn);
    }
  }

  const createScriptTagAndAppendToBody = ({ src, async, defer, id }) => {
    if (document.getElementById(id)) {
      return;
    }
    const scriptTag = document.createElement('script');
    scriptTag.id = id;
    scriptTag.async = async;
    scriptTag.defer = defer;
    scriptTag.type = 'text/javascript';
    scriptTag.src = src;
    document.body.appendChild(scriptTag);
    $log(`The ${src} is injected now. ${new Date().toLocaleString()}`);
  };

  const initialScriptForRoute = (routeName) => {
    for (const scriptName in scripts) {
      const { src, immediateInjectOn, id, async, defer } = scripts[scriptName];
      const isRouteMatchedForImmediateInjection =
        (Array.isArray(immediateInjectOn) && immediateInjectOn.includes(routeName)) || immediateInjectOn === '*';
      if (isRouteMatchedForImmediateInjection) {
        createScriptTagAndAppendToBody({ src, async, defer, id });
      } else {
        setTimeout(() => {
          createScriptTagAndAppendToBody({ src, async, defer, id });
        }, 8000);
      }
    }
  };

  onDocumentIsReady(() => initialScriptForRoute(route.name));

  app.router.afterEach((to, from) => {
    const isRouteChanged = !!to.name && !!from.name;
    if (isRouteChanged) {
      initialScriptForRoute(to.name);
    }
  });
};
