export default ({ $config }, inject) => {
  const hubspotScriptTag = document.createElement('script');
  hubspotScriptTag.async = true;
  hubspotScriptTag.defer = true;
  hubspotScriptTag.type = 'text/javascript';
  hubspotScriptTag.id = 'hs-script-loader';
  hubspotScriptTag.src = $config.HUBSPOT_CHAT_BOX;

  const hubspotData = {
    divTag: null,
  };

  inject('hubspot', {
    setup: () => {
      document.body.appendChild(hubspotScriptTag);
      if (hubspotData.divTag) {
        document.body.appendChild(hubspotData.divTag);
      }
    },
    remove: () => {
      const toRemoveHubspotScriptTag = document.getElementById('hs-script-loader');
      if (toRemoveHubspotScriptTag) {
        toRemoveHubspotScriptTag.parentNode.removeChild(toRemoveHubspotScriptTag);
      }
      const toRemoveHubspotDivTag = document.getElementById('hubspot-messages-iframe-container');
      if (toRemoveHubspotDivTag) {
        // save div tag in plugin
        hubspotData.divTag = toRemoveHubspotDivTag;
        document.body.removeChild(toRemoveHubspotDivTag);
      }
    },
  });
};
