# Site

[![master pipeline status](https://gitlab.com/jackwestin/website/frontend/site/badges/master/pipeline.svg)](https://gitlab.com/jackwestin/website/frontend/site/-/commits/master)
[![develop pipeline status](https://gitlab.com/jackwestin/website/frontend/site/badges/develop/pipeline.svg)](https://gitlab.com/jackwestin/website/frontend/site/-/commits/develop)

## Installation

The project dependencies contain private npm packages like `@jackwestin/uikit`. You need to authenticate yourself first to install them.

1. You need to generate a [Personal Access Token](https://gitlab.com/-/profile/personal_access_tokens)
2. Authenticate with a personal access token:

```sh
npm config set @jackwestin:registry https://gitlab.com/api/v4/packages/npm/

npm config set '//gitlab.com/api/v4/packages/npm/:_authToken' "<your_token>"

npm config set '//gitlab.com/api/v4/projects/:_authToken' "<your_token>"
```
Then you will be able to install dependencies.
```sh
yarn install
```
Read more about Uikit package: https://gitlab.com/jackwestin/website/frontend/uikit

## Build Setup

```bash
# install dependencies
$ yarn install

# serve with hot reload at localhost:3000
$ yarn dev

# build for production and launch server
$ yarn build
$ yarn start

# generate static project
$ yarn generate
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).

### Lint
Please add these settings to your editor:
- VSCode
```js
"editor.formatOnSave": false,
"editor.formatOnPaste": false,
"editor.formatOnType": false,
"editor.codeActionsOnSave": {
    "source.fixAll.eslint": true
},
"eslint.options": {
    "extensions": [".html", ".js", ".vue", ".jsx"]
},
"eslint.validate": [ "html", "vue", "javascript"]
```

### Commit conventions
- feat: A new feature
- fix: A bug fix
- docs: Changes to documentation
- style: Formatting, missing semi colons, etc; no code change
- refactor: Refactoring production code
- test: Adding tests, refactoring test; no production code change
- chore: Updating build tasks, package manager configs, etc; no production code change

You can lint the codes with `yarn lint --fix`
