FROM nginx:alpine

LABEL Maintainer="Afkari <afkari.1370@gmail.com>" Description="jackwestin.com - nuxt - site"

COPY dist /usr/share/nginx/html
COPY docker-config/nginx.conf /etc/nginx/conf.d/default.conf
