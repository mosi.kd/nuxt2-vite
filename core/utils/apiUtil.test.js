import { escapeParam } from '~/core/utils/apiUtil';

describe('sanitizeString', () => {
  const letterRegex = /^(\w( \w)*)*$/;

  test('Should sanitize tags', () => {
    const inputs = ['<script>', '<script></script>', '<html>', '<form>', '<input>'];
    inputs.forEach((input) => {
      const sanitizedInput = escapeParam(input);
      expect(letterRegex.test(sanitizedInput)).toBe(true);
    });
  });

  test('Should sanitize values that has not include letters', () => {
    const inputs = [' a b  c ', '1+3', ' 1-4', '3*5 ', 'a + d'];
    inputs.forEach((input) => {
      const sanitizedInput = escapeParam(input);
      expect(letterRegex.test(sanitizedInput)).toBe(true);
    });
  });

  test('Should length less than 51', () => {
    const array = new Uint32Array(51);
    const input = array.join('');
    expect(escapeParam(input)).toBe(undefined);
  });
});
