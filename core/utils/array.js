/**
 * groups items of an array by specific key
 * @returns grouped items in abject
 * @param {Object[]} array
 * @param {string} key
 */
export const groupBy = (array, key) => {
  try {
    return array.reduce((rv, x) => {
      (rv[x[key]] = rv[x[key]] || []).push(x);
      return rv;
    }, {});
  } catch {
    return null;
  }
};
