export const DECK_IMAGE_RATIO = 2.1;

export const BREAK_POINTS = {
  XL: 1280,
  LG: 1024,
  MD: 768,
  LARGE: 1280,
  WIDE: 1920,
};

export const RECENT_SEARCH_LOCAL_STORAGE_KEY = 'platform/setRecentSearches';

export const CARD_TYPES = {
  BASIC: {
    value: '0',
    name: 'Basic',
  },
  CLOZE: {
    value: '1',
    name: 'Cloze',
  },
};

export const DISCOUNT_TYPES = {
  FIXED: 'FIXED',
  PERCENTAGE: 'PERCENTAGE',
};

export const QUIZ_FEATURES = {
  // GENERAL
  TIMER: 'Timer',
  NEXT: 'Next',
  PREV: 'Prev',
  PAGINATION: 'Pagination',
  REVIEW_ALL: 'ReviewAll',
  REVIEW_MARKED: 'ReviewMarked',
  REVIEW_INCOMPLETE: 'ReviewIncomplete',
  // MCAT
  PAUSE: 'Pause',
  END_EXAM: 'EndExam',
  FLAG: 'Flag',
  NAVIGATION: 'Navigation',
  HIGHLIGHT: 'Highlight',
  STRIKE: 'Strike',
  PERIODIC_TABLE: 'PeriodicTable',
  END_SECTION: 'EndSection',
  REVIEW_FLAGGED: 'ReviewFlagged',
  REVIEW_SCREEN: 'ReviewScreen',
  // DAT
  END: 'End',
  YES: 'Yes',
  NO: 'No',
  MARK: 'Mark',
  EXHIBIT: 'Exhibit',
  REVIEW: 'Review',
};

export const TRUSTED_SITES_LOCAL_STORAGE_KEY = 'trustedSites';
