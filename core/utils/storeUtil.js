import Vue from 'vue';
import Vuex, { Store } from 'vuex';
Vue.use(Vuex);
export const makeStore = (...args) => {
  const store = new Store(...args);
  return store;
};
