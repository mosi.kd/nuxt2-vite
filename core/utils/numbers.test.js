import { inRange } from './numbers';

describe('Numbers Utils', () => {
  test('should number to be in range', () => {
    expect(inRange(400, 400, 499)).toBe(true);
    expect(inRange(400, 400, 500)).toBe(true);
  });

  test('should number not to be in range', () => {
    expect(inRange(400, 500, 600)).toBe(false);
  });
});
