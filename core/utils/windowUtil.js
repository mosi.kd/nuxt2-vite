// https://developer.mozilla.org/en-US/docs/Web/API/Window/open#best_practices

export const redirect = (to, newTab) => {
  const a = document.createElement('a');
  a.href = to;
  if (newTab) {
    a.target = '_blank';
  }
  document.body.appendChild(a);
  a.click();
  document.body.removeChild(a);
};

let windowObjectReference = null;
export const openFFPromotionPopup = (url, title, params, callback) => {
  windowObjectReference = window.open(url, title, params);
  if (windowObjectReference) {
    windowObjectReference.moveTo(0, 0);
    windowObjectReference.onload = () => {
      windowObjectReference.onunload = () => {
        callback();
      };
    };
  } else {
    // open a new tab on any error
    redirect(url, true);
  }
};
