export function verifyPhoneNumberChain(options = {}) {
  if (this.$auth.user && this.$auth.user.is_phone_number_verified) {
    return { success: true };
  }
  return {
    success: false,
    action: () => this.$nuxt.$emit(this.$config.event.show_verify_mobile_modal, { show: true, ...options }),
  };
}
