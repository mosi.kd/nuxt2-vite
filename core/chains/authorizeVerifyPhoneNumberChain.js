export function authorizeVerifyPhoneNumberChain(options = {}) {
  if (this.$auth.user && !this.$auth.user.need_verify_number) {
    return { success: true };
  }
  return {
    success: false,
    action: () => this.$nuxt.$emit(this.$config.event.show_verify_mobile_modal, { show: true, ...options }),
  };
}
