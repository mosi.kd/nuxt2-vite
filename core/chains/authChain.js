export function authChain() {
  if (this.$auth.loggedIn) {
    return { success: true };
  }
  return {
    success: false,
    action: () => this.$nuxt.$emit(this.$config.event.show_login, { value: true }),
  };
}
