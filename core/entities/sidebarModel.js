export const sidebarModel = {
  show: false,
  fixed: false,
  isContainerFull: false,
  dynamicComponent: {
    name: null,
    props: {},
  },
};
