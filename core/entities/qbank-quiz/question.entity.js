import { randomFixedInteger } from '~/core/utils/numbers';

export default class QuestionEntity {
  constructor(json) {
    const requiredKeys = [
      'id',
      'passage_id',
      'seen',
      'countable',
      'question',
      'component',
      'navigable',
      'received_from_server',
      'marked',
    ];

    this.id = json.id ?? randomFixedInteger(10);
    this.passage_id = json.passage_id ?? null;
    this.component = json.component ?? '';
    this.seen = json.seen ?? false;
    this.question = json.question ?? '';
    this.countable = json.countable ?? true;
    this.navigable = json.navigable ?? true;
    this.received_from_server = json.received_from_server ?? false;
    this.marked = json.marked ?? false;

    Object.keys(json).forEach((key) => {
      if (requiredKeys.includes(key)) {
        return;
      }
      this[key] = json[key];
    });
  }
}
