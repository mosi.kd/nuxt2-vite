import { randomFixedInteger } from '~/core/utils/numbers';

export default class SectionEntity {
  constructor(json) {
    const requiredKeys = ['id', 'component'];

    this.id = json.id ?? randomFixedInteger(10);
    this.component = json.component ?? '';

    Object.keys(json).forEach((key) => {
      if (requiredKeys.includes(key)) {
        return;
      }
      this[key] = json[key];
    });
  }
}
