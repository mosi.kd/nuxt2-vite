import { randomFixedInteger } from '~/core/utils/numbers';

export default class PassageEntity {
  constructor(json) {
    const requiredKeys = ['id', 'component'];

    this.id = json.id ?? randomFixedInteger(10);
    this.component = json.component ?? '';
    this.received_from_server = json.received_from_server ?? true;

    Object.keys(json).forEach((key) => {
      if (requiredKeys.includes(key)) {
        return;
      }
      this[key] = json[key];
    });
  }

  get isEmpty() {
    const pureText = this.passage.replace(/(<([^>]+)>)/gi, '');
    return ['', '[HIDDEN_PASSAGE]'].includes(pureText);
  }

  get hasContent() {
    return !this.isEmpty;
  }

  get isDiscreteSet() {
    return this.passage_type === 'DISCRETESET';
  }
}
