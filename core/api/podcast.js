export default ($axios) => ({
  fetchLastPodcast() {
    return $axios.get('/api/caap/podcasts', { params: { latest: true } });
  },
});
