export default ($axios) => ({
  toggleLessonComplete(lessonId) {
    return $axios.post(`/api/caap/lessons/${lessonId}/completion/toggle`);
  },

  playRecord(recordId) {
    return $axios.get(`/api/caap/classroom/generate-tmp-url/${recordId}`);
  },
});
