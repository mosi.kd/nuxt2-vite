export default ($axios) => ({
  fetchProfile() {
    return $axios.get('/api/caap/me');
  },

  updateProfile(params) {
    return $axios.put('/api/caap/instructor/profile', {
      headline: params.headline,
      bio: params.bio,
    });
  },
});
