export default ({ primaryClass, outlineClass }) => ({
  props: {
    type: {
      type: String,
      default: 'button',
    },
    outline: {
      type: Boolean,
      default: false,
    },
    disabled: {
      type: Boolean,
      default: false,
    },
    rounded: {
      type: Boolean,
      default: false,
    },
    large: {
      type: Boolean,
      default: false,
    },
    loading: {
      type: Boolean,
      default: false,
    },
    padding: {
      type: String,
      default: '',
    },
  },
  computed: {
    tagName() {
      if (this.$attrs.to) {
        return 'nuxt-link';
      }
      if (this.$attrs.href) {
        return 'a';
      }
      return 'button';
    },
    classList() {
      const defaultClasses = 'jw-btn flex items-center justify-center font-gotham';
      return [defaultClasses, this.isOutline, this.isRounded, this.isLarge, this.isDisabled];
    },
    isOutline() {
      return this.outline ? this.isOutlineAndDisabled : this.isNotOutlineAndDisabled;
    },
    isOutlineAndDisabled() {
      return outlineClass;
    },
    isNotOutlineAndDisabled() {
      return primaryClass;
    },
    isRounded() {
      return this.rounded ? 'rounded-full' : 'rounded-md';
    },
    isLarge() {
      return this.large ? this.isLargeRounded : this.isNotLargeRounded;
    },
    isLargeRounded() {
      return this.rounded
        ? `${this.padding || 'px-10 py-3.5'} text-base`
        : `${this.padding || 'px-6 py-3.5'} text-base`;
    },
    isNotLargeRounded() {
      return this.rounded ? `${this.padding || 'px-10 py-3'} text-sm` : `${this.padding || 'px-6 py-3'} text-sm`;
    },
    isDisabled() {
      return this.disabled || this.loading ? 'cursor-not-allowed' : '';
    },
  },
  methods: {
    onClicked() {
      this.$emit('clicked');
      this.$emit('click');
    },
  },
});
