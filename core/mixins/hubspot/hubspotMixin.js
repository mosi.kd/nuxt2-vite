export default {
  data() {
    return {
      blackList: {
        regexes: [
          /^flashcard/,
          /courses-slug-learn/,
          /courses-slug-study/,
          /sessions-id/,
          /class-id-checkout/,
          /tier-id-checkout/,
          /login/,
          /student-verify/,
          /student-verification/,
          /register/,
          /exams-test/,
          /exams-tests-id/,
          /^exams-app-daily/,
          /^daily-app-section-daily/,
        ],
      },
    };
  },

  watch: {
    '$route.name': {
      handler(routeName, oldRouteName) {
        if (this.shouldIncludeHubspot(oldRouteName) && !this.shouldIncludeHubspot(routeName)) {
          // if old route is not in black list and current route is in black list remove hubspot div
          this.$hubspot.remove();
        } else if (!this.shouldIncludeHubspot(oldRouteName) && this.shouldIncludeHubspot(routeName)) {
          // if old route is in black list and current route is not in black list setup hubspot
          this.$hubspot.setup();
        }
      },
    },
  },

  methods: {
    shouldIncludeHubspot(routeName) {
      return !this.blackList.regexes.some((blackRegex) => {
        return blackRegex.test(routeName);
      });
    },
  },
  mounted() {
    if (this.shouldIncludeHubspot(this.$route.name)) {
      this.$hubspot.setup();
    }
  },
  beforeDestroy() {
    if (!this.shouldIncludeHubspot(this.$route.name)) {
      this.$hubspot.remove();
    }
  },
};
