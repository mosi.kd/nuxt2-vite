import { mapActions, mapGetters } from 'vuex';

import { distanceFromNowInMiliSeconds, isDateAfterNow, isDateBeforeNow } from '@/core/management/dateTimeManagement';
export const enrollSessionMixin = {
  data() {
    return {
      isCountDownEnded: false,
      meetingURL: null,
      confirmDialog: false,
    };
  },
  computed: {
    isSavedSessionAndExpired() {
      return this.session.encore_is_live && isDateBeforeNow(this.session.end_date);
    },

    isStartDateAfterNow() {
      return isDateAfterNow(this.session.start_date);
    },

    isStartDateBeforeNow() {
      return isDateBeforeNow(this.session.start_date);
    },

    isSessionStarted() {
      return this.session?.has_started ?? false;
    },

    isFinished() {
      // is a saved encore?
      if (this.session.encore_is_live) {
        return this.isSavedSessionAndExpired;
      }
      // is a live encore
      return this.session.has_finished;
    },

    isUserEnrolled() {
      return this.session?.has_student_enrolled ?? false;
    },

    startDateMiliseconds() {
      return distanceFromNowInMiliSeconds(this.session.start_date);
    },
    ...mapGetters('session', ['session']),
  },
  watch: {
    meetingURL(value) {
      if (!value) {
        return;
      }
      location.replace(value);
    },
  },
  methods: {
    onCountDownEnded() {
      this.isCountDownEnded = true;
    },

    async logoutFromAnotherDevice() {
      await this.onJoinSession(true);
    },

    async onJoinSession(logout = false) {
      try {
        if (this.session.is_user_attending && !logout) {
          this.confirmDialog = true;
          return;
        }
        const res = await this.joinSession({
          id: this.session.id,
          logout,
        });
        if (res?.data?.meeting_url) {
          this.meetingURL = res.data.meeting_url;
        } else {
          // an error occurred
          this.$nuxt.error({ statusCode: 500 });
          this.$bugsnag.notify(`An error occurred on the join session button: ${res}`);
        }
      } catch (error) {
        this.$nuxt.error({ statusCode: 500 });
      }
    },

    async onEnrollSession() {
      try {
        await this.enrollSession(this.session.id);
      } catch (error) {
        this.redirectToLanding();
      }
    },

    async onFetchSession() {
      try {
        await this.fetchSession(this.session.id);
      } catch (error) {
        this.redirectToLanding();
      }
    },

    redirectToLanding() {
      this.$router.push({
        name: 'sessions-id',
        params: { slug: this.session.id },
      });
    },
    ...mapActions('session', ['enrollSession', 'joinSession', 'fetchSession']),
  },
  mounted() {
    if (this.session.is_user_able_to_enroll) {
      this.onEnrollSession();
    }
  },
};
