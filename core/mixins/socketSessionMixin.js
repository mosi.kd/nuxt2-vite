import { mapGetters } from 'vuex';
import io from 'socket.io-client';

export const socketSessionMixin = {
  data() {
    return {
      $socket: null,
    };
  },

  mounted() {
    this.initSocket();
  },

  computed: {
    ...mapGetters({ socketSession: 'session/session' }),
  },

  methods: {
    initSocket() {
      if (this.$socket || !this.$auth.loggedIn) {
        return;
      }
      if (!window.origin) {
        window.origin =
          window.location.protocol +
          '//' +
          window.location.hostname +
          (window.location.port ? ':' + window.location.port : '');
      }
      this.$socket = io(window.origin, {
        path: '/et/socket.io',
        query: {
          id: this.$auth.user.id,
          uuid: 'uuid_' + this.$auth.user.id,
          authorization: 'token_' + this.$auth.user.id,
        },
      });
    },

    trackTime(status) {
      if (!this.$socket) {
        return;
      }
      if (process.browser) {
        this.$socket.emit('event', {
          user_id: this.$auth.user.id,
          session_id: this.socketSession.id,
          event: status,
        });
      }
    },

    disconnect() {
      this.trackTime('user-left');
      this.$socket.disconnect();
      this.$socket = null;
    },
  },

  beforeDestroy() {
    this.disconnect();
  },
};
