export default {
  methods: {
    async fetchSessionsByTypes(typesArrayArg) {
      const typesArray = typeof typesArrayArg === 'string' ? [typesArrayArg] : [...typesArrayArg];
      const axiosRequests = [];

      // push all requests to axiosRequests array
      typesArray.forEach((type) => {
        axiosRequests.push(
          this.$axios.get('/api/caap/sessions', {
            params: {
              type,
              sort: 'start_date',
              order: 'asc',
              per_page: 6,
            },
          }),
        );
      });

      return await Promise.all(axiosRequests).then((responseArray) => {
        let sessions = [];

        // merge all type responses
        responseArray.forEach((response) => {
          const data = response?.data || [];
          sessions = [...sessions, ...data];
        });

        // sort by start_date
        const sortedSessions = sessions.sort((a, b) => (a.start_date > b.start_date ? 1 : -1));
        return sortedSessions;
      });
    },
  },
};
