import { mapActions, mapGetters } from 'vuex';
export const autoEnrollToSessionMixin = {
  data() {
    return {
      loading: false,
      showJoinDialog: false,
    };
  },
  computed: {
    hasWaitingList() {
      return this.course?.has_session_waiting_list;
    },
    ...mapGetters('session', ['sessions', 'firstFreeAvailableSession', 'freeSessions', 'hasFreeSessions']),
    ...mapGetters('course', ['course']),
  },
  methods: {
    async autoCountIn() {
      this.$scrollTo('#sessions', {
        easing: 'ease',
        duration: 400,
      });
      if (this.firstFreeAvailableSession) {
        try {
          await this.enrollSession(this.firstFreeAvailableSession.id);
          this.showJoinDialog = true;
          this.$router.push(this.$route.path);
          return;
        } catch (error) {}
      }
      if (this.hasWaitingList && !this.hasFreeSessions) {
        await this.addUserToWaittingList(this.course.slug);
        this.$nuxt.$emit(this.$config.event.show_trial_session_dialog, true);
      }
      this.$router.push(this.$route.path);
    },

    async autoCountInProcess() {
      if (!this.$route.query.join || this.$route.query.join !== 'next-free-session') {
        return;
      }
      if (!this.$auth.loggedIn) {
        this.$router
          .replace({
            name: 'login',
            query: { redirect: `${this.$route.path}?join=next-free-session` },
          })
          .catch(() => {});
      } else {
        await this.autoCountIn();
      }
    },

    closeJoinDialog() {
      this.showJoinDialog = false;
    },
    ...mapActions('session', ['enrollSession']),
    ...mapActions('course', ['addUserToWaittingList']),
  },
  created() {
    // listen to any loggin event
    this.$nuxt.$on(this.$config.event.logged_in, () => {
      if (!this.$route.query.join || this.$route.query.join !== 'next-free-session') {
        return;
      }
      this.autoCountIn();
    });
  },
  watch: {
    sessions: {
      handler() {
        this.autoCountInProcess();
      },
    },
  },
};
