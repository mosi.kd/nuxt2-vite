import moment from 'moment';

export default {
  data() {
    return {
      cards: {
        learning: [],
        new: [],
        due: [],
        graduate: [],
      },
      cardsLog: [],
      hashId: null,
      currentCard: null,
      changeTracker: 1,
    };
  },

  computed: {
    hasPrevious() {
      return this.cardsLog.length > 0;
    },
    cardsCount() {
      const cardsCount = {};
      Object.entries(this.cards).forEach(([categoryKey, categoryValue]) => {
        cardsCount[categoryKey] = categoryValue.length;
      });
      return this.changeTracker && cardsCount;
    },
  },

  methods: {
    async getDeckCardCategories(subdeckId = null) {
      try {
        const actionPath = subdeckId ? 'flashcard/getSubdeckStudySessionCards' : 'flashcard/getDeckStudySessionCards';
        const data = await this.$store.dispatch(actionPath, {
          deckId: this.deck.id,
          subdeckId,
        });
        Object.entries(data).forEach(([categoryKey, categoryValue]) => {
          const categoryName = categoryKey.replace('_cards', '');
          this.cards[categoryName] = categoryValue;
        });
      } catch (err) {
        throw new Error(err);
      }
    },
    async getHashIdByCardId(cardId) {
      const response = await this.$store.dispatch('flashcard/fetchSingleCardHash', {
        deckId: this.deck.id,
        cardId,
      });
      if (!response) {
        this.$jwToast.error(this.$config.errors.general_error);
        return;
      }
      this.hashId = response?.hash;
    },
    async turnNextCard() {
      const nextTurnCard = await this.getNextTurnCard(this.currentCard?.category);
      if (nextTurnCard) {
        this.currentCard = nextTurnCard;
        this.$gtag('event', 'page_view', {
          deck: this.deck.title,
          page_location: `${location.origin}/flashcard/${this.deck.slug}/learn?card=${this.currentCard.id}`,
        });
        return this.getHashIdByCardId(this.currentCard.id);
      }
      this.currentCard = null;
    },
    getNextTurnCard(current) {
      if (!['new', 'due'].includes(current)) {
        // by default search in ready learning-cards only
        const nextTurnLearningCard = this.getNextTurnLearningCard(true);
        if (nextTurnLearningCard) {
          return nextTurnLearningCard;
        }
      }
      const nextTurnNotLearningCard = this.getNextTurnNotLearningCard(current);
      if (nextTurnNotLearningCard) {
        return nextTurnNotLearningCard;
      }
      // here is not any new or due card, so search in all learning-cards
      return this.getNextTurnLearningCard(false);
    },
    getNextTurnLearningCard(readyOnly) {
      let nextTurnLearningCard;
      nextTurnLearningCard = this.cards.learning.find((card) =>
        this.isLearningCardReady(card.modificationTimestamp, card.nextInterval),
      );
      if (nextTurnLearningCard) {
        nextTurnLearningCard.category = 'learning';
      } else if (!readyOnly && this.cardsCount.learning > 0) {
        nextTurnLearningCard = this.getFirstCardFromCategory('learning');
      }
      return nextTurnLearningCard;
    },
    isLearningCardReady(modificationTimestamp, nextInterval) {
      if (!modificationTimestamp || !nextInterval) {
        return true;
      }
      const readyDatetime = this.addNextIntervalDuration(modificationTimestamp, nextInterval);
      return readyDatetime < moment().format();
    },
    addNextIntervalDuration(dateTime, duration) {
      const [timeAmount, unitFlag] = duration.split(' ');
      let addedDateTime = dateTime;
      let unit;
      switch (unitFlag) {
        case 's':
          unit = 'seconds';
          break;
        case 'm':
          unit = 'minutes';
          break;
        case 'h':
          unit = 'hours';
          break;
        case 'd':
          unit = 'days';
          break;
        case 'mon':
          unit = 'months';
          break;
        case 'year':
          unit = 'years';
          break;
        default:
          unit = null;
      }
      if (unit) {
        addedDateTime = moment(dateTime).add(unit, timeAmount).format();
      }
      return addedDateTime;
    },
    // by default set current 'new', it makes due higher priority
    getNextTurnNotLearningCard(current = 'new') {
      const nextPriority = {
        due: 'new',
        new: 'due',
      };
      if (!nextPriority[current]) {
        current = 'due';
      }

      if (this.cardsCount[nextPriority[current]] > 0) {
        return this.getFirstCardFromCategory(nextPriority[current]);
      }

      return this.cardsCount[current] > 0 && this.getFirstCardFromCategory(current);
    },

    getFirstCardFromCategory(category) {
      return {
        ...this.cards[category][0],
        category,
      };
    },

    forceUpdate() {
      this.changeTracker++;
    },
    getCard(cardId, category) {
      return this.cards[category].find((card) => card.id === cardId);
    },
    deleteCard(cardId, category) {
      this.cards[category] = this.cards[category].filter((card) => card.id !== cardId);
    },
    insertCardIntoCategory(card, category, position) {
      const cardsInCategory = [...this.cards[category]];
      if (position > -1) {
        cardsInCategory.splice(position, 0, card);
      } else {
        // when position is not specific, push card to end of category
        cardsInCategory.push(card);
      }
      this.cards[category] = cardsInCategory;
    },
    moveCardToAnotherCategory(targetCategory, newRepeatInterval, nextInterval) {
      if (!this.currentCard.first_repeat_interval) {
        this.currentCard.first_repeat_interval = this.currentCard.repeat_interval;
      }
      const modifiedCard = {
        ...this.currentCard,
        repeat_interval: newRepeatInterval,
        category: targetCategory,
        shouldOverride: false,
        modificationTimestamp: moment().format(),
        nextInterval,
      };
      this.deleteCard(modifiedCard.id, this.currentCard.category);
      this.insertCardIntoCategory(modifiedCard, targetCategory);
      this.cardsLog.push({
        target: targetCategory,
        source: this.currentCard.category,
        cardId: modifiedCard.id,
      });
      this.forceUpdate();
    },
    async previousCard() {
      const lastAction = this.cardsLog.pop();
      const card = this.getCard(lastAction?.cardId, lastAction?.target);
      card.shouldOverride = true;
      card.repeat_interval = card.first_repeat_interval;
      this.deleteCard(lastAction?.cardId, lastAction?.target);
      this.insertCardIntoCategory(card, lastAction?.source, 0);
      this.forceUpdate();
      await this.turnNextCard();
    },
    resetCardLogs() {
      this.cardsLog = [];
    },
  },
};
