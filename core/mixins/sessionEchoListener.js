import { mapActions } from 'vuex';

export const sessionEchoListener = {
  computed: {
    isStudentEnrolled() {
      return this.session.has_student_enrolled;
    },
    isStarted() {
      return this.session.has_started;
    },
  },

  watch: {
    'session.has_student_enrolled': {
      immediate: true,
      deep: true,
      handler(enrolled) {
        enrolled === true ? this.connect() : this.disconnect();
      },
    },
  },

  methods: {
    connect() {
      try {
        if (this.$echo) {
          this.$echo.private(`session.${this.session.id}`).listen('.session.status', (e) => {
            if (e.status === 'created' && this.session.has_student_enrolled) {
              this.setStarted(this.session.id);
            }
          });
        }
      } catch (error) {}
    },

    disconnect() {
      if (!this.$echo) {
        return;
      }
      this.$echo.leave('.session.status');
    },
    ...mapActions('session', ['setStarted']),
  },

  destroyed() {
    this.disconnect();
  },
};
