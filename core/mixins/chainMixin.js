export default {
  methods: {
    chainMethods(...middlewares) {
      const executer = () => {
        return async (callback) => {
          window.middlewareCallback = callback;
          let isValid = true;
          for (const middleware of middlewares) {
            const { success, action = () => {} } = middleware();
            if (success) {
              continue;
            }
            isValid = false;
            action();
            break;
          }

          if (isValid) {
            await callback();
            this.$nuxt.$off(this.$config.event.chain_succeeded);
          }
        };
      };

      this.$nuxt.$on(this.$config.event.chain_succeeded, () => {
        return executer()(window.middlewareCallback);
      });

      return executer();
    },
  },
};
