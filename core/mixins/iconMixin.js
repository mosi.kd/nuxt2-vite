export default {
  props: {
    size: {
      type: [Number, String],
      default: '1em',
    },
  },
};
