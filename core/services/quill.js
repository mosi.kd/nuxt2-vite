export default class QuillService {
  setEditor(quillInstance) {
    this.quillInstance = quillInstance;
    return this;
  }

  insertImageButton(fileInput) {
    const toolbar = this.quillInstance.getModule('toolbar');
    toolbar.addHandler('image', () => {
      fileInput && fileInput.click();
    });
  }

  async uploadImage({ event, deckId, getQuillInstance }) {
    const file = event.target.files[0];
    const formData = new FormData();
    formData.append('file', file);
    const quillSelector = event.target.getAttribute('data-for-editor');
    const quillInstance = getQuillInstance(quillSelector);
    const currentIndex = quillInstance.selection.lastRange.index ?? 0;
    const response = await window.$nuxt.$axios.post(`/api/v1/flashcard/decks/${deckId}/files`, formData);
    event.target.value = '';
    quillInstance.insertEmbed(currentIndex, 'image', response.data.file_path);
    quillInstance.setSelection(currentIndex + 1, 0);
  }

  async uploadAudio({ event, ref, thisRefs, deckId, getQuillInstance }) {
    const formData = new FormData();
    const file = thisRefs[ref][0].files[0];
    formData.append('file', file);
    const { data } = await window.$nuxt.$axios.post(`/api/v1/flashcard/decks/${deckId}/files`, formData);
    event.target.value = '';
    this.appendAudioPath(data.file_path, getQuillInstance, event);
  }

  appendAudioPath(path, getQuillInstance, event) {
    const quillSelector = event.target.getAttribute('data-for-editor');
    const quillInstance = getQuillInstance(quillSelector);
    const currentIndex = quillInstance?.getLength() - 1 || 0;
    quillInstance.insertText(currentIndex, `[sound:${path}]`);
    quillInstance.setSelection(quillInstance?.getLength(), 0);
  }
}
