export default class ClozerService {
  setEditor(editor) {
    this.editor = editor;
    return this;
  }

  addClozeItem({ color, content, hint }) {
    const index = this.editor.getLength() - 1;
    const text = hint ? `{{c1::${content}::${hint}}}` : `{{c1::${content}}}`;
    this.editor.insertText(index, text, { color });
  }

  addEventListenerForClozerButton(openClozerModal) {
    const clozeElement = document.querySelector('.ql-cloze');
    clozeElement?.addEventListener('click', openClozerModal);
  }
}
