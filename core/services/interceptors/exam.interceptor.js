import QuestionEntity from '~/core/entities/qbank-quiz/question.entity';

export default (quiz, sectionType) => {
  const infoQuestions = [];
  const tutorialQuestions = [];
  const voidQuestions = [];

  if (sectionType === 'info') {
    // initial info section questions
    for (let i = 1; i < 4; i++) {
      infoQuestions.push(
        new QuestionEntity({
          component: `exam-info-step-${i}`,
          navigable: false,
        }),
      );
    }
  } else if (sectionType === 'tutorial') {
    // initial tutorial section questions
    for (let i = 1; i < 11; i++) {
      const questionContents = {
        2: 'Navigate the Question 2',
        3: 'Select an Answer 3',
        4: 'Use the Scroll Feature 4',
        5: 'Use the Highlight Feature 5',
        6: 'Use the Strikethrough Feature 6',
        7: 'Refer to the Periodic Table 7',
        8: 'Use the Flag for Review Feature 8',
        9: 'Use the Section Review Feature 9',
      };
      tutorialQuestions.push(
        new QuestionEntity({
          component: `exam-tutorial-step-${i}`,
          question: questionContents[i] ?? '',
          navigable: [2, 3, 4, 5, 6, 7, 8, 9].includes(i), // set navigable to false for the first and the last steps
        }),
      );
    }
  } else if (sectionType === 'void') {
    // initial void section question
    voidQuestions.push(
      new QuestionEntity({
        component: 'exam-void-step',
        navigable: false,
      }),
    );
  } else if (sectionType === 'question') {
    // add a welcome-step to each question-section
    quiz.questions = [
      new QuestionEntity({
        component: 'exam-welcome-step',
        countable: false,
        navigable: false,
      }),
      ...quiz.questions,
      new QuestionEntity({
        id: 'review-step',
        component: 'exam-review-questions-step',
        countable: false,
        navigable: false,
      }),
    ];
  }

  quiz.questions = [...quiz.questions, ...infoQuestions, ...tutorialQuestions, ...voidQuestions];
  return quiz;
};
