export class AnalyticsECommerce {
  constructor({ amount, item, extraItemData = {} }) {
    this.amount = amount;
    this.item = item;
    this.extraItemData = extraItemData;

    this.initialProductItem();
  }

  initialProductItem() {
    this.productItem = {
      item_id: this.item.id,
      item_name: this.item.title,
      ...this.extraItemData,
    };
  }

  // https://developers.google.com/analytics/devguides/collection/ga4/reference/events#add_to_cart
  dispatchAddToCartEvent() {
    window.$nuxt.context.app.$gtag('event', 'add_to_cart', {
      currency: 'USD',
      value: this.amount,
      items: [this.productItem],
    });
    return this;
  }

  // https://developers.google.com/analytics/devguides/collection/ga4/reference/events#purchase
  dispatchPurchaseEvent({ transactionId, extraEventData = {} }) {
    window.$nuxt.context.app.$gtag('event', 'purchase', {
      currency: 'USD',
      transaction_id: transactionId,
      value: this.amount,
      ...extraEventData,
      items: [this.productItem],
    });
    return this;
  }
}
