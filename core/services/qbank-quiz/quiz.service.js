import examInterceptor from '../interceptors/exam.interceptor';
import datInterceptor from '../interceptors/dat.interceptor';
import { HighlightService } from './highlight/highlight.service';
import PassageEntity from '~/core/entities/qbank-quiz/passage.entity';
import QuestionEntity from '~/core/entities/qbank-quiz/question.entity';
import SectionEntity from '~/core/entities/qbank-quiz/section.entity';
import types from '~/store/quiz/types';

const enums = {
  exam: 'Exam',
  dat: 'Dat',
  daily: 'Daily',
  question: 'Question',
  break: 'Break',
  tutorial: 'Tutorial',
  info: 'Info',
  void: 'Void',
};

export default class QuizService {
  constructor(context) {
    this.context = context;
  }

  /**
   * Get getter value of quiz module
   * @param {String} getterName the name of getter
   * @returns getter value
   * @author HN
   */
  quizGetters(getterName) {
    return this.context.store.getters[`quiz/${getterName}`];
  }

  /**
   * We have sections array in the store. The user wants to start test with specific section id.
   * In some case we know the section id but in other cases we have only the question id that user wants to see.
   * If we know just the question id, we call an API to find it section id and we can modify question index and section index with them.
   * If we know both question id and section id that user want to see, we should modify indexes only.
   * After that we can set the active section index to store.
   * @param {Object} quiz The quiz object that received from the server
   * @param {Number} testId The test id
   * @param {Number} questionId The question id that we want to start test from it
   * @param {Number|undefined} sectionId The section id that we want to start test from it
   * @author HN
   */
  async initialSectionIndexForSpecificQuestion(testId, questionId, sectionId = null) {
    if (!sectionId) {
      const { section_id, parent_section_id } = await this.context.store.dispatch('quiz/fetchSectionIdByQuestion', {
        testId,
        questionId,
      });
      sectionId = parent_section_id || section_id;
    }
    const sectionIndex = this.quizGetters('studyableSections').findIndex((section) => section.section_id === sectionId);
    if (sectionIndex === -1) {
      throw new Error('Section Not Found.');
    }
    await this.context.store.commit(`quiz/${types.SET_SECTION_INDEX}`, sectionIndex);
  }

  /**
   * We have questions array in the store. The user wants to start test with specific question id.
   * So we should find it index from our array and set it index in the store as the active question index.
   * @param {Object} quiz The quiz object that received from the server
   * @param {Number} questionId The question id that we want to start test from it
   * @author HN
   */
  async initialQuestionIndexForSpecificQuestion(quiz, questionId) {
    const questionIndex = quiz.questions.findIndex((question) => question.id === questionId);
    if (questionId && questionIndex === -1) {
      throw new Error('Question Not Found.');
    }
    await this.context.store.commit(`quiz/${types.SET_QUESTION_INDEX}`, questionIndex);
  }

  /**
   * This method is run only when we fetch the quiz from the server
   * Sort the sections that exist in the quiz object and add component property to section items based on it type and quiz type.
   * @param {Object} quiz The quiz object that received from the server
   * @returns the quiz that it sections are modified
   */
  modifySections = (quiz) => {
    // sort sections by order
    const orderedSections = quiz.sections.sort((a, b) => a.order - b.order);

    // Initial section component based on quiz type and section type
    const quizType = enums[quiz.test.test_type.toLowerCase()].toLowerCase();
    const sections = orderedSections.map((section) => {
      const sectionType = enums[section.type.toLowerCase()].toLowerCase();
      return new SectionEntity({
        ...section,
        component: `${quizType}-${sectionType}-section`,
      });
    });

    // bind to quiz again
    return { ...quiz, sections };
  };

  /**
   * This method is run after each API calls for fetching the questions and passages (before starting each question base section)
   * Because all sections that are question base have a last step that user can see all questions and chosen answers we should add
   * a question to them.
   * Add component property for each question based on quiz type
   * @param {Object} quiz The quiz object that received from the server
   * @param {Array} questions The sections that we fetched from the server for current section
   * @returns the modified questions
   * @author HN
   */
  modifyQuestions = (quiz, questions) => {
    // Initial component property of questions that are come from server based on quiz type
    return questions.map((question) => {
      const quizType = enums[quiz.test.test_type.toLowerCase()].toLowerCase();
      return new QuestionEntity({
        ...question,
        received_from_server: true,
        component: `quiz-${quizType}-question-step`,
      });
    });
  };

  /**
   * This method is run after each API calls for fetching the questions and passages (before starting each question base section)
   * Add component property for each passage based on quiz type
   * @param {Object} quiz The quiz object that received from the server
   * @param {Array} passages The passages that we fetched from the server for current section
   * @returns modified passages
   * @author HN
   */
  modifyPassages = (quiz, passages) => {
    // Initial passage component based on quiz type
    const quizType = enums[quiz.test.test_type.toLowerCase()].toLowerCase();
    passages = passages.map(
      (question) =>
        new PassageEntity({
          ...question,
          component: `quiz-${quizType}-passage`,
        }),
    );
    return passages;
  };

  /**
   * This method is trn after active section changed
   * We should have an interceptor for each quiz type and apply them based on current quiz type and return modified quiz.
   * Because each quiz type have some logics that another types haven't so we need interceptor for them.
   * Interceptor that is place for changes that have only for specific quiz type
   * @param {Object} quiz The quiz object that received from the server
   * @returns modified quiz object
   * @author HN
   */
  applyInterceptors(quiz) {
    const sectionType = this.quizGetters('activeSectionType');
    const activeSection = this.quizGetters('activeSection');

    switch (quiz.test.test_type.toLowerCase()) {
      case 'exam':
        return examInterceptor(quiz, sectionType, activeSection);
      case 'dat':
        return datInterceptor(quiz, sectionType, activeSection);
    }
    return quiz;
  }

  /**
   * Call an API every 15 seconds to update test_duration on the server.
   * @param {Number} testId The test id
   * @author HN
   */
  startBackgroundTimer(testId) {
    const isTestFinished = this.quizGetters('isTestFinished');
    if (isTestFinished) {
      return;
    }
    this.stopBackgroundTimer();
    this.backgroundTimerInterval = setInterval(() => {
      const sectionId = this.quizGetters('activeSection')?.section_id;
      this.context.$api.quiz.syncBackgroundTimer(testId, sectionId);
    }, 15000);
  }

  stopBackgroundTimer() {
    clearInterval(this.backgroundTimerInterval);
    this.backgroundTimerInterval = null;
  }

  isActionQuestionIntervalRunning() {
    return !!this.activeQuestionInterval;
  }

  /**
   * Start timer on activeQuestion every time it changes.
   * @author HN
   */
  startActiveQuestionTimer() {
    const isTestFinished = this.quizGetters('isTestFinished');
    if (isTestFinished || this.isActionQuestionIntervalRunning()) {
      return;
    }
    this.activeQuestionInterval = setInterval(() => {
      this.context.store.dispatch('quiz/increaseActiveQuestionTimer');
    }, 1000);
  }

  /**
   * stop the interval of question
   * @author HN
   */
  stopActiveQuestionTimer() {
    clearInterval(this.activeQuestionInterval);
    this.activeQuestionInterval = null;
  }

  /**
   * redirect to exam landing page by accessing the needed slugs from state
   * @author HN
   */
  redirectToExamLandingPage() {
    const quiz = this.quizGetters('quiz');
    const { app_slug: app, daily_slug: daily } = quiz;
    this.context.redirect({ name: 'exams-app-daily', params: { app, daily } });
  }

  /**
   * Finish the current test
   * @author HN
   */
  async finishTest() {
    try {
      const test = this.quizGetters('test');
      const activeSection = this.quizGetters('activeSection');

      this.stopBackgroundTimer();
      this.stopActiveQuestionTimer();

      await this.context.store.dispatch('quiz/finishSection', {
        testId: test.id,
        sectionId: activeSection.section_id,
        examSectionId: activeSection.id,
      });
      await this.context.store.dispatch('quiz/finishTest', test.id);

      this.context.$gtag('event', 'qbank_usage', { type: 'complete_test' });
    } catch (err) {
      throw new Error(err);
    }
  }

  /**
   * Call google analytics on each activeQuestion
   * @author HN
   */
  callActiveQuestionAnalytics() {
    const quiz = this.quizGetters('quiz');
    const activeQuestion = this.quizGetters('activeQuestion');
    const activeSection = this.quizGetters('activeSection');
    const activeQuestionCounterNumber = this.quizGetters('activeQuestionCounterNumber');

    if (activeQuestion.received_from_server) {
      const { app_slug, section_slug, daily_slug } = quiz;
      const basePath = `${app_slug}/${section_slug}/${daily_slug}/${activeSection.slug}`;
      const questionPath = `${basePath}-${activeQuestionCounterNumber}`;
      this.context.$gtag('event', 'pageViewDaily', {
        event_category: 'pageViewDaily',
        event_label: questionPath,
        value: questionPath,
      });
    }
  }

  /**
   * Initial questions and passages for specific question id or first section
   * If questionId provided, we should find the section of that questionId
   * @param {Null|Number} questionId the question id
   * @author HN
   */
  async initialQuestionsAndPassages(questionId = null, sectionId = null) {
    const test = this.quizGetters('test');

    const testId = test.id;
    const quiz = { ...this.quizGetters('quiz'), questions: [], passages: [] };

    const isQuizStartedWithSpecificSection = !!sectionId;
    const isQuizStartedWithSpecificQuestion = !!questionId;

    try {
      if (isQuizStartedWithSpecificSection || isQuizStartedWithSpecificQuestion) {
        await this.initialSectionIndexForSpecificQuestion(testId, questionId, sectionId);
      }

      if (this.quizGetters('activeSectionType') === 'question') {
        const { data } = await this.context.$api.quiz.fetchPassagesAndQuestions({
          testId,
          sectionId: this.quizGetters('activeSection').section_id,
        });
        quiz.questions = this.modifyQuestions(quiz, data.questions);
        quiz.passages = this.modifyPassages(quiz, data.passages);
      }

      const modifiedQuiz = this.applyInterceptors(quiz);
      this.context.store.commit(`quiz/${types.SET_QUIZ}`, modifiedQuiz);

      if (isQuizStartedWithSpecificQuestion) {
        await this.initialQuestionIndexForSpecificQuestion(modifiedQuiz, questionId);
      }

      this.startBackgroundTimer(testId);
    } catch (err) {
      throw new Error(err);
    }
  }

  get highlight() {
    return new HighlightService(this.context);
  }
}
