export const configs = {
  // Date
  defaultDate: 'YYYY-MM-DD',

  // player
  player: {
    options: {
      controls: [
        'play-large',
        'restart',
        'rewind',
        'play',
        'fast-forward',
        'progress',
        'duration',
        'mute',
        'volume',
        'settings',
        'pip',
        'fullscreen',
      ],
      vimeo: {
        loop: false,
      },
    },
  },

  errors: {
    general_error: 'Something went wrong. Please reload and try again.',
  },

  // Stripe style
  stripeStyle: {
    base: {
      color: '#32325d',
      fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
      fontSmoothing: 'antialiased',
      fontSize: '16px',
      '::placeholder': {
        color: '#aab7c4',
      },
    },
    invalid: {
      color: '#fa755a',
      iconColor: '#fa755a',
    },
  },

  // in miliseconds
  resendTimer: 110000,

  // event bus
  event: {
    player: {
      toggle_sidebar: 'player_toggle_sidebar',
      is_sidebar_open: 'player_is_sidebar_open',
    },
    sidebar: {
      toggle_sidebar: 'sidebar_toggle_sidebar',
      enable_toggle: 'sidebar_enable_toggle',
    },
    session: {
      toggle_session_enrollment: 'toggle_session_enrollment',
      sessions_page_tab_change: 'sessions_page_tab_change',
    },
    show_login: 'show_login',
    enable_player_progress: 'enable_player_progress',
    enroll_to_session: 'enroll_to_session',
    logged_in: 'logged_in',
    show_trial_session_dialog: 'show_trial_session_dialog',
    subscribe_clicked: 'subscribe_clicked',
    unsubscribe_clicked: 'unsubscribe_clicked',
    show_verify_mobile_modal: 'show_verify_mobile_modal',
    chain_succeeded: 'chain_succeeded',
    verified_mobile_and_test_date: 'verified_mobile_and_test_date',
    quiz: {
      layout: {
        reset: 'quiz_layout_reset',
        remove_section: 'quiz_layout_remove_section',
      },
      close_timer_modals: 'quiz_close_timer_modals',
      highlight: 'quiz_highlight_annotate',
    },
    toggle_cookie_banner: 'toggle_cookie_banner',
    show_external_link_dialog: 'show_external_link_dialog',
  },
};
