import moment from 'moment';
import { durationSum, isMandatoryDurationLessThanSelectedDuration, getNextWeekOfStudyPlan } from './studyProvider';
import { lessons } from '~/__fakedata__/lessons';
import { randomInRange } from '~/core/utils/numbers';

describe('durationSum', () => {
  test('should return sum of lessons duration', () => {
    const sum = durationSum([...lessons.filter((item) => !item.completed)]);
    expect(parseInt(sum)).toBe(85);
  });
});

describe('isMandatoryDurationLessThanSelectedDuration', () => {
  test('should return true', () => {
    const lessonsDuration = durationSum([...lessons]);
    const result = isMandatoryDurationLessThanSelectedDuration(lessonsDuration, 32, 4);
    expect(result).toBe(true);
  });

  test('should return false', () => {
    const lessonsDuration = durationSum([{ duration: '02:10:00' }, { duration: '02:05:00' }]);
    const sum = isMandatoryDurationLessThanSelectedDuration(lessonsDuration, 1, 2);
    expect(sum).toBe(false);
  });

  it('should return true if the student needs >= 4 extra hours to finish the course at the desired date in learn page', () => {
    const now = moment();
    const remainingDaysToDeadline = randomInRange(15, 25);
    const hourPerDay = randomInRange(10, 30) / 10;
    const canStudy = Math.ceil(remainingDaysToDeadline * hourPerDay);
    const lessonsDuration = canStudy + randomInRange(1, 4);
    const deadlineDate = now.add(remainingDaysToDeadline + 1, 'day');
    const timePerWeek = hourPerDay * 7;
    const result = isMandatoryDurationLessThanSelectedDuration({
      deadlineDate,
      isStudyPlanEnabled: true,
      lessonsDuration,
      timePerWeek,
    });
    expect(result).toBe(true);
  });

  it('should return false if the student needs >= 4 extra hours to finish the course at the desired date in study page', () => {
    const result = isMandatoryDurationLessThanSelectedDuration({
      deadlineDate: moment().add(23, 'day'),
      isStudyPlanEnabled: false,
      lessonsDuration: 26,
      timePerWeek: 8,
    });
    expect(result).toBe(false);
  });
});

describe('getNextWeekOfStudyPlan', () => {
  it('should return next same day of current date', () => {
    const nextDay = getNextWeekOfStudyPlan('2021-10-11').format('YYYY-MM-DD');
    expect(nextDay).toBe('2021-10-17');
  });
});
