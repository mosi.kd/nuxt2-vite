import has from 'lodash/has';
const timeFormat = require('hh-mm-ss');

export const getCourseTotalTime = (sections) => {
  if (!sections) {
    return 0;
  }
  let totalTime = 0;
  sections.forEach((section) => {
    totalTime += getSectionTotalTime(section);
  });
  return totalTime;
};

export const getCourseOnDemandVideo = (sections) => {
  if (!sections) {
    return 0;
  }
  let totalTime = 0;
  sections.forEach((section) => {
    totalTime += getSectionTotalTime(section, 'article');
  });
  return totalTime;
};

export const getSectionTotalTime = (section, type = null) => {
  if (!has(section, 'lessons')) {
    return 0;
  }
  let totalTime = 0;
  section.lessons
    .filter((lesson) => {
      return type ? lesson.content_type !== type : lesson;
    })
    .forEach((lesson) => {
      if (lesson.duration) {
        totalTime += timeFormat.toMs(lesson.duration, 'hh:mm:ss');
      }
    });
  return totalTime;
};

export const getCourseLessonsCount = (sections) => {
  if (!sections) {
    return 0;
  }
  let count = 0;
  Object.keys(sections).forEach((key) => {
    if (sections[key].lessons) {
      count += Object.keys(sections[key].lessons).length || 0;
    }
  });
  return count;
};

export const getCompletedLessons = (sections) => {
  if (!sections) {
    return [];
  }
  const lessons = [];
  if (Array.isArray(sections)) {
    sections.forEach((section) => {
      section.lessons.forEach((lesson) => {
        if (lesson.completed) {
          lessons.push(lesson.id);
        }
      });
    });
  }
  return lessons;
};

export const getSectionCompletedLessons = (section) => {
  if (!section || !section.lessons) {
    return [];
  }
  const lessons = [];
  section.lessons.forEach((lesson) => {
    if (lesson.completed) {
      lessons.push(lesson.id);
    }
  });
  return lessons;
};

export const getSectionLectureCounts = (section) => {
  if (!section && has(section, 'lessons')) {
    return;
  }
  const count = { lecture: 0, homework: 0, workshop: 0 };
  section.lessons.forEach((lesson) => {
    switch (lesson.type) {
      case 'lecture':
        count.lecture += 1;
        break;
      case 'homework':
        count.homework += 1;
        break;
      case 'workshop':
        count.workshop += 1;
        break;
    }
  });
  return count;
};

export const getCourseLectureCounts = (sections) => {
  if (!sections) {
    return;
  }
  const counts = [];
  sections.forEach((section) => {
    counts.push(getSectionLectureCounts(section));
  });
  return counts;
};

export const getLectureCountsString = (count) => {
  let stringFormat = '';
  Object.keys(count).forEach((key) => {
    if (count[key] > 0) {
      stringFormat += `${count[key]} ${count[key] > 1 ? key + 's' : key} • `;
    }
  });
  return stringFormat;
};
