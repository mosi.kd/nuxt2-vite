import moment from 'moment';
const timeFormatUtil = require('hh-mm-ss');

/**
 * Returns distance from now to a certain date in miliseconds
 * @param {String} date
 */
export const distanceFromNowInMiliSeconds = (date) => {
  const now = moment.utc();
  const started = moment.utc(date);
  const diff = moment.duration(now.diff(started), 'ms');
  return Math.abs(diff);
};

/**
 * Returns distance from now to a certain date in seconds
 * @param {String} date
 */
export const diffBetweenNowAndDate = (date) => {
  const started = moment.utc(date);
  return moment.utc().local().diff(started, 's');
};

/**
 * Returns milisconds from a duration format: hh:mm:ss
 * @param {String} date
 */
export const convertDurationToMS = (duration) => {
  return timeFormatUtil.toMs(duration, 'hh:mm:ss');
};

/**
 * Returns true if date is before now
 * @param {String} date
 */
export const isDateBeforeNow = (date) => {
  return moment.utc(date).isBefore(moment.utc());
};

/**
 * Returns true if date is after now
 * @param {String} date
 */
export const isDateAfterNow = (date) => {
  return moment.utc(date).isAfter(moment.utc());
};
