import moment from 'moment';

import { distanceFromNowInMiliSeconds, isDateAfterNow, isDateBeforeNow } from '@/core/management/dateTimeManagement';

describe('dateTimeManagement', () => {
  test('should distanceFromNowInMiliSeconds return miliseconds', () => {
    const miliseconds = distanceFromNowInMiliSeconds('2020-11-13 20:02:00');
    expect(miliseconds).toBeGreaterThan(0);
  });

  test('should isDateBeforeNow return true', () => {
    const isBefore = isDateBeforeNow(moment.utc().subtract(10, 'ms'));
    expect(isBefore).toBe(true);
  });

  test('should isDateBeforeNow return false', () => {
    const isBefore = isDateBeforeNow(moment.utc().add(10, 'ms'));
    expect(isBefore).toBe(false);
  });

  test('should isDateAfterNow return true', () => {
    const isAfter = isDateAfterNow(moment.utc().add(10, 'ms'));
    expect(isAfter).toBe(true);
  });

  test('should isDateAfterNow return false', () => {
    const isAfter = isDateAfterNow(moment.utc().subtract(10, 'ms'));
    expect(isAfter).toBe(false);
  });
});
