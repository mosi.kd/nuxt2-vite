export const parseVideo = (url) => {
  let type = null;
  url.match(
    /(http:|https:|)\/\/(player.|www.)?(vimeo\.com|youtu(be\.com|\.be|be\.googleapis\.com))\/(video\/|embed\/|watch\?v=|v\/)?([A-Za-z0-9._%-]*)(&\S+)?/,
  );
  if (RegExp.$3.includes('youtu')) {
    type = 'youtube';
  } else if (RegExp.$3.includes('vimeo')) {
    type = 'vimeo';
  }
  return {
    type,
    id: RegExp.$6,
  };
};

export const isVimeoOrYoutube = (url) => {
  if (!url) {
    return false;
  }
  const video = parseVideo(url);
  return video.type === 'vimeo' || video.type === 'youtube';
};

export const generateUrl = (url) => {
  const video = parseVideo(url);
  if (video.type === 'vimeo') {
    return `https://player.vimeo.com/video/${video.id}`;
  } else {
    return `${url}&playsinline=1&autoplay=1`;
  }
};
