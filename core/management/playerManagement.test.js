import { isVimeoOrYoutube } from '~/core/management/playerManagement';

describe('playerManagement', () => {
  test('should isVimeoOrYoutube return true', () => {
    const isYoutubeOrVimeo = isVimeoOrYoutube('https://www.jackwestin.com');
    expect(isYoutubeOrVimeo).toBe(false);
  });

  test('should isVimeoOrYoutube return true', () => {
    const isYoutube = isVimeoOrYoutube(
      'https://www.youtube.com/watch?v=4QTQu-qo5GY&list=RDMM4QTQu-qo5GY&start_radio=1',
    );
    expect(isYoutube).toBeTruthy();
  });

  test('should isVimeoOrYoutube return true', () => {
    const isVimeo = isVimeoOrYoutube('https://vimeo.com/479633680');
    expect(isVimeo).toBeTruthy();
  });

  test('should isVimeoOrYoutube return false on null value', () => {
    const isVimeo = isVimeoOrYoutube(null);
    expect(isVimeo).toBe(false);
  });
});
