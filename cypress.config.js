/// <reference types="cypress" />
const { defineConfig } = require('cypress');
require('dotenv').config();

module.exports = defineConfig({
  e2e: {
    baseUrl: process.env.SITE_URL,
    pageLoadTimeout: 1000000,
    setupNodeEvents() {
      // implement node event listeners here
    },
  },
});
